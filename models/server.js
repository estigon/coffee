const express = require('express');
const cors = require('cors');

class Server {

    constructor() {
        this.port = process.env.PORT;
        this.app = express();

        //Paths
        this.userPath = '/api/usuarios';

        //Middlewares
        this.middlewares();

        //Routes
        this.routes();
    }

    routes() {
        this.app.use(this.userPath, require('./../routes/user'));
    }

    middlewares() {

        // Serialize data from body
        this.app.use( express.json() );

        // CORS
        this.app.use( cors() );
        // public folder
        this.app.use( express.static('public') );
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log(`listening the port ${this.port}`);
        });
    }
}

module.exports = Server;