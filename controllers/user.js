const { request, response } = require('express');

const getUser = (req, res) => {
    const queryParams = req.query;

    res.json({ 
        message: 'get request',
        queryParams
    });
};

const putUser = (req = request, res = response) => {
    const {id} = req.params;
    res.json({ 
        message: 'put request',
        id
    });
};

const postUser = (req, res) => {
    const {name, age} = req.body;

    res.status(201).json({ 
        name, age
    });
};

const deleteUser = (req, res) => {
    const {id} = req.params;
    res.json({ 
        message: 'delete request',
        id
    });
};

module.exports = { getUser, putUser, postUser, deleteUser };